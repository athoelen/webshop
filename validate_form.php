<?php

/*
  if (isset($_POST['register'])) {

  $errors = "";
  $gebruikersnaam = "";
  $wachtwoord = "";
  $straat = "";
  $plaats = "";
  $nummer = "";
  $email = "";
  $gsm = "";

  if ($_POST['gebruikersnaam'] != "") {
  $_POST['gebruikersnaam'] = filter_var($_POST['gebruikersnaam'], FILTER_SANITIZE_STRING);
  if ($_POST['gebruikersnaam'] == "") {
  $errors .= 'Gebruik een geldige gebruikersnaam.<br/><br/>';
  }
  } else {
  $errors .= 'Geef je gebruikersnaam.<br/>';
  }



  if ($_POST['wachtwoord'] != "") {
  $_POST['wachtwoord'] = filter_var($_POST['wachtwoord'], FILTER_SANITIZE_STRING);
  if ($_POST['wachtwoord'] == "") {
  $errors .= 'Fout wachtwoord.<br/><br/>';
  }
  } else {
  $errors .= 'Geef je wachtwoord.<br/>';
  }
  if ($_POST['naam'] != "") {
  $_POST['naam'] = filter_var($_POST['naam'], FILTER_SANITIZE_STRING);
  if ($_POST['naam'] == "") {
  $errors .= 'Geef je naam.<br/><br/>';
  }
  } else {
  $errors .= 'Geef je naam.<br/>';
  }

  if ($_POST['voornaam'] != "") {
  $_POST['voornaam'] = filter_var($_POST['voornaam'], FILTER_SANITIZE_STRING);
  if ($_POST['voornaam'] == "") {
  $errors .= 'Geef je voornaam.<br/>';
  }
  } else {
  $errors .= 'Geef je voornaam.<br/>';
  }


  if ($_POST['straat'] != "") {
  $_POST['straat'] = filter_var($_POST['straat'], FILTER_SANITIZE_STRING);
  if ($_POST['straat'] == "") {
  $errors .= 'geef een geldige straatnaam.<br/><br/>';
  }
  } else {
  $errors .= 'Geef je straatnaam.<br/>';
  }




  if ($_POST['nummer'] != "") {
  $_POST['nummer'] = filter_var($_POST['nummer'], FILTER_VALIDATE_INT);
  if ($_POST['nummer'] == "") {
  $errors .= 'geef een geldig nummer.<br/><br/>';
  }
  } else {
  $errors .= 'Geef een geldig adresnummer.<br/>';
  }


  if ($_POST['plaats'] != "") {
  $_POST['plaats'] = filter_var($_POST['plaats'], FILTER_SANITIZE_STRING);
  if ($_POST['plaats'] == "") {
  $errors .= 'geef een geldige plaatsnaam.<br/><br/>';
  }
  } else {
  $errors .= 'Geef je plaatsnaam.<br/>';
  }

  function validateEmail($email) {
  $result = array();
  $error = null;

  if ($email != '') {
  $email = filter_var($email, FILTER_SANITIZE_EMAIL);

  if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
  $error = 'Please enter a valid email address:';
  }
  } else {
  $error = 'Please enter your email address:';
  }

  $result['value'] = $email;
  $result['error'] = $error;

  return $result;
  }

  if ($_POST['gsm'] != "") {
  $_POST['gsm'] = filter_var($_POST['gsm'], FILTER_VALIDATE_INT);
  if ($_POST['gsm'] == "") {
  $errors .= 'geef een geldig GSM-nummer.<br/><br/>';
  }
  } else {
  $errors .= 'Geef je GSM-nummer.<br/>';
  }

  if (!isset($_POST['voorwaarden'])) {
  echo "Algemen voorwaarden zijn niet geaccepteerd.";
  }





  /*
  if ($_POST['homepage'] != "") {
  $homepage = filter_var($_POST['straat'], FILTER_SANITIZE_URL);
  if (!filter_var($homepage, FILTER_VALIDATE_URL)) {
  $errors .= "$homepage is <strong>NOT</strong> a valid URL.<br/><br/>";
  }
  } else {
  $errors .= 'Please enter your home page.<br/>';
  }

  if ($_POST['message'] != "") {
  $_POST['message'] = filter_var($_POST['message'], FILTER_SANITIZE_STRING);
  if ($_POST['message'] == "") {
  $errors .= 'Please enter a message to send.<br/>';
  }
  } else {
  $errors .= 'Please enter a message to send.<br/>';
  }

  if (!$errors) {


  echo "Wij hebben u met succes kunnen registreren, ga verder naar <a href='home.php'>de homepagina</a>!!<br/><br/>";
  } else {
  echo '<div style="color: red">' . $errors . '<br/></div>';
  }
  }
 */
?>
<?php

function validateEmail($email) {
    $result = array();
    $error = null;

    if ($email != '') {
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $error = 'Please enter a valid email address:';
        }
    } else {
        $error = 'Please enter your email address:';
    }

    $result['value'] = $email;
    $result['error'] = $error;

    return $result;
}

function validatePassword($password, $passwordConfirmation = false) {
    $result = array();
    $error = null;

    if ($password != '') {
        $password = filter_var($password, FILTER_SANITIZE_STRING);

        if ($password == '') {
            $error = 'Please enter a valid password:';
        }
    } else {
        // Begin met de default waarde
        $error = 'Please enter your password:';

        if ($passwordConfirmation == true) {
            // Wijs de uitzondering toe.
            $error = 'Please enter your password for confirmation:';
        }
    }

    $result['value'] = $password;
    $result['error'] = $error;

    return $result;
}

function validateString($string, $label) {
    $result = array();
    $error = null;

    if ($string != '') {
        $string = filter_var($string, FILTER_SANITIZE_STRING);
    } else {
        $error = "Please enter $label:";
    }

    $result['value'] = $string;
    $result['error'] = $error;

    return $result;
}

function checkbox() {

    $result = array();
    $error = null;
    
    if (!isset($_POST['accept_terms'])) {
        
           $error = "ga akkoord";
      
    }
    
    $result['terms'] = $terms;
    $result['error'] = $error;
    
    return $result;
}
?>

