<?php

class Users {
    protected $userID;
    protected $userRole;
    Protected $city;
    Protected $email;
    Protected $firstName;
    protected $lastName;
    protected $streetAdress;
    protected $city;
    protected $postalCode;
    protected $countryID;
    

    Public function setUserID($userID){

        $this->userID = $userID;
    }
    Public function getUserID(){
        return $this->userID;
    }
    Public function setUserRole($userRole){
        $this->UserRole = $userRole;
    }
    Public function getUserRole(){
        return $this->userRole;
    }
    Public function setEmail($email){
        $this->email = $email;
    }
    Public function getEmail(){
        return $this->email;
    }
    Public function setFirstName($firstName){
        $this->firstName = $firstName;
    }
    Public function getFirstName(){
        return $this->firstName;
    }
    Public function setLastName($lastName){
        $this->lastName = $lastName;
    }
    Public function getLastName(){
        return $this->lastName;
    }
    Public function setStreetAdress($streetAdress){
        $this->streetAdress = $streetAdress;
    }
    Public function getStreetAdress(){
        return $this->streetAdress;
    }
    Public function setCity($city){
        $this->city = $city;
    }
    Public function getCity(){
        return $this->city;
    }
    Public function setPostalCode($postalCode){
        $this->postalCode = $postalCode;
    }
    Public function getPostalCode(){
        return $this->postalCode;
    }
    Public function setCountryID($countryID){
        $this->countryID = $countryID;
    }
    Public function getCountryID(){
        return $this->countryID;
    }
    }
?>
