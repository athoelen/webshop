<?php


class Manager {
    Protected $NextReview;
    Protected $UserRole;
    
    Public function setNextReview($NextReview){
        $this->NextReview = $NextReview;
    }
    Public function getNextReview(){
        return $this->NextReview;
    }
    
    Public function setUserRole($UserRole){
        $this->UserRole = $UserRole;
    }
    Public function getUserRole(){
        return $this->UserRole;
    }
}

?>
