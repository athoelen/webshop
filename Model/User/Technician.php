<?php


class Technician {
    Protected $Certified;
    Protected $UserRole;
    
    Public function setCertified($Certified){
        $this->Certified = $Certified;
    }
    Public function getCertified(){
        return $this->Certified;
    }
    
    Public function setUserRole($UserRole){
        $this->UserRole = $UserRole;
    }
    Public function getUserRole(){
        return $this->UserRole;
    }
}

?>
