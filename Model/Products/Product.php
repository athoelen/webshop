<?php


class Product {
    Protected $ProductID;
    Protected $Name;
    Protected $Image;
    Protected $Description;
    
    Public function setProductID ($ProductID){
        $this->ProductID = $ProductID;
    }
    Public function getProductID(){
        return $this->ProductID;
    }
    
    Public function setName ($Name){
        $this->Name = $Name;
    }
    Public function getName(){
        return $this->Name;
    }
   Public function setImage ($Image){
        $this->Image = $Image;
    }
    Public function getImage(){
        return $this->Image;
    }
    Public function setDescription ($Description){
        $this->Description = $Description;
    }
    Public function getDescription(){
        return $this->Description;
    }

}
?>
