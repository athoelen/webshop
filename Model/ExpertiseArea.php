<?php

class ExpertiseArea {
    Protected $ProductID;
    Protected $UserID;
    Protected $ExpLevel;
    Protected $Notes;
    
    Public function setProductID ($ProductID){
        $this->ProductID = $ProductID;
    }
    Public function getProductID(){
        return $this->ProductID;
    }
    
    Public function setUserID ($UserID){
        $this->UserID = $UserID;
    }
    Public function getUserID(){
        return $this->UserID;
    }
   Public function setExpLevel ($ExpLevel){
        $this->ExpLevel = $ExpLevel;
    }
    Public function getExpLevel(){
        return $this->ExpLevel;
    }
    Public function setNotes ($Notes){
        $this->Notes = $Notes;
    }
    Public function getNotes(){
        return $this->Notes;
    }

}

?>
