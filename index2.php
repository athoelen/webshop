<?php
include_once __DIR__ . '/utilities/validation.php';
include_once __DIR__ . '/utilities/formHelpers.php';

$validationResults = array();
$formHasErrors = false;

if (isset($_POST['login'])) {
    // De gebruiker heeft de login button geklikt.
    $validationResults['email'] = validateEmail($_POST['email']);
    $validationResults['password'] = validatePassword($_POST['password']);

    foreach ($validationResults as $result) {
        if (isset($result['error'])) {
            $formHasErrors = true;
            break;
        }
    }

    // Als er geen validatie fouten zijn, kunnen we de gegevens opslaan en de
    // gebruiker doorverwijzen naar de home pagina.
    if ($formHasErrors === false) {
        // Sla de waarden op in de database.
        header('Location: home.php');
    }
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Sign in &middot; Twitter Bootstrap</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="assets/css/bootstrap.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 40px;
                padding-bottom: 40px;
                background-color: #f5f5f5;
            }

            .form-signin, .register {
                max-width: 350px;
                padding: 19px 29px 29px;
                margin: 0 auto 20px;
                background-color: #fff;
                border: 1px solid #e5e5e5;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
            }
            .form-signin .form-signin-heading,
            .form-signin .checkbox,
            .register .register-heading {
                margin-bottom: 10px;
            }
            .form-signin input[type="text"],
            .form-signin input[type="password"] {
                font-size: 16px;
                height: auto;
                margin-bottom: 15px;
                padding: 7px 9px;
            }

            .form-signin label.error {
                color: red;
            }
        </style>
        <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="assets/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="assets/ico/favicon.png">
    </head>

    <body>
        <div class="container">

            <form class="form-signin" action="index.php" method="post">
                <h2 class="form-signin-heading">Please sign in</h2>

                <?php echo getErrorLabel($validationResults, 'email'); ?>
                <input type="text" class="input-block-level" name="email" id="email" placeholder="Email address" value="<?php echo getValue($validationResults, 'email'); ?>">

                <?php echo getErrorLabel($validationResults, 'password'); ?>
                <input type="password" class="input-block-level" name="password" id="password" placeholder="Password">

                <label class="checkbox">
                    <input type="checkbox" name="remember-me" id="remember-me" value="remember-me"> Remember me
                </label>

                <button class="btn btn-large btn-primary" name="login" id="login" type="submit">Sign in</button>
            </form>

            <div class="register">
                <h2 class="register-heading">New at our Webshop?</h2>
                <a href="registration.php" class="btn btn-large btn-primary">Register now</a>
            </div>

        </div> <!-- /container -->

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="assets/js/jquery.js"></script>
        <script src="assets/js/bootstrap-transition.js"></script>
        <script src="assets/js/bootstrap-alert.js"></script>
        <script src="assets/js/bootstrap-modal.js"></script>
        <script src="assets/js/bootstrap-dropdown.js"></script>
        <script src="assets/js/bootstrap-scrollspy.js"></script>
        <script src="assets/js/bootstrap-tab.js"></script>
        <script src="assets/js/bootstrap-tooltip.js"></script>
        <script src="assets/js/bootstrap-popover.js"></script>
        <script src="assets/js/bootstrap-button.js"></script>
        <script src="assets/js/bootstrap-collapse.js"></script>
        <script src="assets/js/bootstrap-carousel.js"></script>
        <script src="assets/js/bootstrap-typeahead.js"></script>

    </body>
</html>
