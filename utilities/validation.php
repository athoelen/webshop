<?php

function validateEmail($email)
{
    $result = array();
    $error = null;

    if ($email != '') {
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $error = 'Vul een geldig e-mailadres in:';
        }
    } else {
        $error = 'Vul je e-mailadres in:';
    }
    
    $result['value'] = $email;
    $result['error'] = $error;
    
    return $result;
}

function validatePassword($password, $passwordConfirmation=false)
{
    $result = array();
    $error = null;

    if ($password != '') {
        $password = filter_var($password, FILTER_SANITIZE_STRING);

        if ($password == '') {
            $error = 'Vul een geldig wachtwoord in:';
        }
    } else {
        // Begin met de default waarde
        $error = 'Vul een wachtwoord in:';

        if ($passwordConfirmation == true) {
            // Wijs de uitzondering toe.
            $error = 'Vul een wachtwoord in voor validatie:';
        }
    }

    $result['value'] = $password;
    $result['error'] = $error;
    
    return $result;
}

function validateString($string, $label)
{
    $result = array();
    $error = null;

    if ($string != '') {
        $string = filter_var($string, FILTER_SANITIZE_STRING);
    } else {
        $error = "Vul je $label in:";
    }

    $result['value'] = $string;
    $result['error'] = $error;
    
    return $result;
}

function validateCheckbox()
{
    $result = array();
    $error = null;
    
    if(!isset($_POST['terms'])){
        $error = "Het is verplicht de Algemene voorwaarden te accepteren.";
    }
    
    $result['error'] = $error;
    
    return $result;
}

?>